import React, {useState} from "react";
var liked=0;
const List = ({ sData, dData }) => {
    
    const[like, setlike] = useState(sData.like)
    const likeClick=()=>{
        if(liked===0){
        liked=1;
        setlike(sData.like+1)
        }
        else{
            liked=0;
            setlike(sData.like);
        }
    }
  return (
    <>
      <div className="sList">
        <div>
          <button onClick={likeClick}>{like}</button>
        </div>
        <div className="tiSt">
          <p id="ST">{sData.title}</p>
          <p id="ST">{sData.subtitle}</p>
        </div>
        <audio  controls/>
        <button id="del-btn" onClick={()=>dData(sData)}>Del</button>
      </div>
    </>
  );
};

export default List;
