import React, {useState} from "react";

const SearchBar = ({Element})=>{

    const [searched,setsearched] = useState("");
    const QClick = ()=>{
        console.log("Quit");
    }
    return (
        
        <div>
            <button onClick={QClick}>Q</button>
            <input className="search-bar" type="text" value={searched} placeholder="Search all tracks" onChange={(event=>{setsearched(event.target.value)})}/>
            <button onClick={()=>Element(searched)}>S</button>
        </div>
    );
}

export default SearchBar;