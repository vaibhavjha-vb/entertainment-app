import React from 'react'
import Info from './List'

const Songs = ({data, delData}) => {
    return (
        <>
        {
        data.map((info)=>{
            return(
                <div>
                    <Info sData={info} dData={delData}/>
                </div>
            )
        })
    }
        </>
    )
}

export default Songs
