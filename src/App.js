import Header from './components/Header'
import SearchBar from './components/SearchBar';
import Data from './components/Data'
import SongsList from './components/Songs'
import React, {useState} from 'react';
import './App.css';

function App() {
  const [data, setData] = useState(Data)

  const SerSong=(Element)=>{
    setData(data.filter((SElement)=>{
      return (Object.values(SElement).join(" ").toLowerCase().includes(Element.toLowerCase()))
    }))
  }

  const DelSong=(ID)=>{
    setData(data.filter((DelID)=>{
      return DelID!==ID;
    }))
  }

  return (
    <div className="App">
      <Header/>
      <SearchBar Element={SerSong}/>
      <SongsList data={data} delData={DelSong}/>
    </div>
  );
}

export default App;
